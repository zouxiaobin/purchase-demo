import cn from './cn'
import en from './en'
import thai from './thai'

const messages = {
  cn: {
    m: cn
  },
  en: {
    m: en
  },
  thai: {
    m: thai
  }
}

export default messages
