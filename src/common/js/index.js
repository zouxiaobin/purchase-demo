export function lang () {
  let t = window.localStorage.getItem('language')
  if (t) return t
  else return 'thai'
}
