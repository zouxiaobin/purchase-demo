import 'babel-polyfill'
import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from 'vuex-persistedstate'

Vue.use(Vuex)

const store = new Vuex.Store({
	state: {
		skuStore: {
			skuList: []
		},
		isMenu: true,
		routerItems: {},
		routerInex: ''
	},
	mutations: {
		addSkuStore(state, ctx) {
			state.skuStore.skuList = [...ctx]
		},
		setisMenu(state, message) {
			state.isMenu = message
		},
		setRouterItems(state, item) {
			state.routerItems = item
		},
		setRouterIndex(state, index) {
			state.routerInex = index
		},
	},
	plugins: [createPersistedState()]
})

export default store