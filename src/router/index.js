import Vue from 'vue'
import Router from 'vue-router'
// 首页
import index from '@/components/d/index'
import orderGoodsList from '@/components/purchasingManagement/orderGoodsList'
import demandList from '@/components/demandList'
import purchaseList from '@/components/purchasingManagement/purchaseList'
import spuList from '@/components/commodityManagement/spuList'
import skuList from '@/components/commodityManagement/skuList'
import supplierList from '@/components/supplierManagement/supplierList'
import defaultSupplierList from '@/components/supplierManagement/defaultSupplierList'
import advancerList from '@/components/financialManagement/advancerList'
import accountPeriodList from '@/components/financialManagement/accountPeriodList'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [{
    path: '',
    name: 'index',
    redirect: 'orderGoodsList',
    component: index,
    meta: {
      title: '采购管理',
      icon: 'lock',
    },
    children: [{
      path: 'orderGoodsList',
      component: orderGoodsList,
      name: 'orderGoodsList',
      meta: {
        title: '订单欠货表',
        icon: 'orderGoodsList',
        noCache: true
      }
    },{
      path: 'demandList',
      component: demandList,
      name: 'demandList',
      meta: {
        title: '需求单列表',
        icon: 'demandList',
        noCache: true
      }
    },{
      path: 'purchaseList',
      component: purchaseList,
      name: 'purchaseList',
      meta: {
        title: '采购单列表',
        icon: 'purchaseList',
        noCache: true
      }
    }]
  },{
    path: '',
    name: '',
    redirect: 'spuList',
    component: index,
    meta: {
      title: '商品管理',
      icon: 'lock',
    },
    children: [{
      path: 'spuList',
      component: spuList,
      name: 'spuList',
      meta: {
        title: 'SPU管理',
        icon: 'spuList',
        noCache: true
      }
    },{
      path: 'skuList',
      component: skuList,
      name: 'skuList',
      meta: {
        title: 'SKU管理',
        icon: 'skuList',
        noCache: true
      }
    }]
  },{
    path: '',
    name: '',
    redirect: 'supplierList',
    component: index,
    meta: {
      title: '供应商管理',
      icon: 'lock',
    },
    children: [{
      path: 'supplierList',
      component: supplierList,
      name: 'supplierList',
      meta: {
        title: '供应商列表',
        icon: 'supplierList',
        noCache: true
      }
    },{
      path: 'defaultSupplierList',
      component: defaultSupplierList,
      name: 'defaultSupplierList',
      meta: {
        title: '默认供应商列表',
        icon: 'defaultSupplierList',
        noCache: true
      }
    }]
  },{
    path: '',
    name: '',
    redirect: 'advancerList',
    component: index,
    meta: {
      title: '财务管理',
      icon: 'lock',
    },
    children: [{
      path: 'advancerList',
      component: advancerList,
      name: 'advancerList',
      meta: {
        title: '预付结算表',
        icon: 'advancerList',
        noCache: true
      }
    },{
      path: 'accountPeriodList',
      component: accountPeriodList,
      name: 'accountPeriodList',
      meta: {
        title: '账期结算表',
        icon: 'accountPeriodList',
        noCache: true
      }
    }]
  }]
})
