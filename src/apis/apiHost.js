// 第三方包
import Vue from 'vue'
import axios from 'axios'
import * as newApis from '@/apis/api'
import md5 from 'js-md5';


// 公共参数
const sessionContext = {
    "entityCode": 1,
    "channel": "h5",
    "serviceCode": "tft-express",
    "postingDateText": "20180704101359", //当前时间戳
    "valueDateText": "20180704101359",   //当前时间戳
    "localDateTimeText": "20180704101359",  //当前时间戳
    "transactionBranch": "TFT_ORG_0000",
    "userId": window.localStorage.getItem('boxId'),
    "password": "1",
    "superUserId": window.localStorage.getItem('boxId'),
    "superPassword": "1",
    "authorizationReason": window.localStorage.getItem('boxToken'),
    "externalReferenceNo": "AC13E60196B212A1D574542442DEC226",
    "userReferenceNumber": "46837E01E6AB318A498217BF048A1C68",  //32大写随机字符
    "originalReferenceNo": "61489F02A37F74D6E73B6BF32F428E88",   
    "marketCode": "1",
    "stepCode": "1",
    "accessSource": "1",
    "accessSourceType": "1",
  }

// 域名
const host = () => {
// 测试
	let dev = true
	return dev ? '/api' : 'http://192.168.5.30:9010'   //线上环境
}

//生成32位数字+字符的随机字符串
const randomString = (len) => {
　　len = len || 32;
　　var $chars = 'ABCDEFGHJKMNPQRSTWXYZabcdefhijkmnprstwxyz2345678';    /****默认去掉了容易混淆的字符oOLl,9gq,Vv,Uu,I1****/
　　var maxPos = $chars.length;
　　var pwd = '';
　　for (let i = 0; i < len; i++) {
　　　　pwd += $chars.charAt(Math.floor(Math.random() * maxPos));
　　}
　　return pwd;
}
const guid = () => {
    function S4() {
       return (((1+Math.random())*0x10000)|0).toString(16).substring(1);
    }
    return (S4()+S4()+S4()+S4()+S4()+S4()+S4()+S4()).toUpperCase()
}

// 获取当前时间戳
const getNowTime = () => {
	let nowTime = new Date().getTime();
	return nowTime
}

// 延迟
const config = {
	defaults: {
	timeout : 30000
	}
}

const postApi = (url,parm,method) => {
	let postUrl = host() + url   //柜机调试
	if(url.indexOf('/bes') != -1 || url.indexOf('/cif') != -1 || url.indexOf('/cos') != -1) {
		postUrl = host() + '/forward' + url
	}
	const newContext = Object.assign(sessionContext)
	  if (parm.serviceCode) {
	    newContext.serviceCode = parm.serviceCode
	    delete parm.serviceCode
	  }
	sessionContext.authorizationReason = window.localStorage.getItem('boxToken')
	sessionContext.userId = window.localStorage.getItem('boxId')
	sessionContext.superUserId = window.localStorage.getItem('boxId')
	sessionContext.userReferenceNumber = guid()
	sessionContext.postingDateText = getNowTime()
    sessionContext.valueDateText = getNowTime()
    sessionContext.localDateTimeText = getNowTime()
    sessionContext.locale = window.localStorage.getItem('language') === 'thai' ? 'th-TH' : 'zh-CN'
	let options = {
		...parm,
		sessionContext
	}
	if(method && method==='put'){
		return axios.put(postUrl,parm,config).then(res => {
			return Promise.resolve(res.data)
		})
	}
	return axios.post(postUrl,options,config).then(res => {
		return Promise.resolve(res.data)
	})
	.catch(() => {
//      this.$store.commit('showToast', {
//        switch: true,
//        text: this.$t('m.networkErr')
//      })
      })
}

// post方法
export default postApi