export const timeDifference = (endTime,langCur) => {
	let startTime_ms = new Date();  //开始时间
	let endTime_ms = Date.parse(new Date(endTime.replace(/-/g, "/")));   // endtime 为结束时间
	let getTime = endTime_ms - startTime_ms
	//计算出相差天数
	var days=Math.floor(getTime/(24*3600*1000))
	 
	//计算出小时数
	var leave1=getTime%(24*3600*1000)    //计算天数后剩余的毫秒数
	var hours=Math.floor(leave1/(3600*1000))
	var mins = Math.floor((leave1 % (1000 * 60 * 60)) / (1000 * 60))
	if(langCur === 'cn'){
		return days + '天' + hours + '小时' + mins + '分钟'
	}else if(langCur === 'thai'){
		return days + 'วัน' + hours + 'เวลา' + mins + 'นาที'
	}else{
		return days + 'Days' + hours + 'Hour' + mins + 'Minute'
	}
}

export const timeOut = (endTime) => {
	let startTime_ms = new Date(); //开始时间
	let endTime_ms = Date.parse(new Date(endTime.replace(/-/g, "/"))); // endtime 为结束时间
	let getTime = endTime_ms - startTime_ms
	// 判断getTime是否大于0
	if (getTime >= 0) {
	//计算出相差天数
	var days = Math.floor(getTime / (24 * 3600 * 1000))
	//计算出小时数
	var leave1 = getTime % (24 * 3600 * 1000) //计算天数后剩余的毫秒数
	var hours = Math.floor(leave1 / (3600 * 1000))
	var mins = Math.floor((leave1 % (1000 * 60 * 60)) / (1000 * 60))
	if (days == 0) {
	return '还剩余' + hours + '小时' + mins + '分'
	}
	return '还剩余' + days + '天' + hours + '小时'
	} else {
	var newTime = Math.abs(getTime)
	var newDay = Math.floor(newTime / (24 * 3600 * 1000))
	var newLeave = newTime % (24 * 3600 * 1000)
	var newHour = Math.floor(newLeave / (3600 * 1000))
	var newMin = Math.floor((newLeave % (1000 * 60 * 60)) / (1000 * 60))
	// 判断是否在一天之内
	if (newDay == 0) {
	return newHour + "小时" + newMin + "分"
	}
	return newDay + '天' + newHour + '小时'
	}
}

export const getTimeDiff= (timeDifference,langCur) => {
	let getTime = timeDifference*1000;
	// 判断getTime是否大于0
	if (getTime >= 0) {
		//计算出相差天数
		var days = Math.floor(getTime / (24 * 3600 * 1000))
		//计算出小时数
		var leave1 = getTime % (24 * 3600 * 1000) //计算天数后剩余的毫秒数
		var hours = Math.floor(leave1 / (3600 * 1000))
		var mins = Math.floor((leave1 % (1000 * 60 * 60)) / (1000 * 60))
		if (days == 0) {
			if(langCur === 'cn'){
				return hours + '小时' + mins + '分钟'
			}else if(langCur === 'thai'){
				return hours + 'ชั่วโมง' + mins + 'นาที'
			}else{
				return hours + 'Hour' + mins + 'Minute'
			}
		}else{
			if(langCur === 'cn'){
				return days + '天' + hours + '小时' + mins + '分钟'
			}else if(langCur === 'thai'){
				return days + 'วัน' + hours + 'เวลา' + mins + 'นาที'
			}else{
				return days + 'Days' + hours + 'Hour' + mins + 'Minute'
			}
		}
	} else {
		var newTime = Math.abs(getTime)
		var newDay = Math.floor(newTime / (24 * 3600 * 1000))
		var newLeave = newTime % (24 * 3600 * 1000)
		var newHour = Math.floor(newLeave / (3600 * 1000))
		var newMin = Math.floor((newLeave % (1000 * 60 * 60)) / (1000 * 60))
		// 判断是否在一天之内
		if (newDay == 0) {
			if(langCur === 'cn'){
				return newHour + '小时' + newMin + '分钟'
			}else if(langCur === 'thai'){
				return newHour + 'ชั่วโมง' + newMin + 'นาที'
			}else{
				return newHour + 'Hour' + newMin + 'Minute'
			}
		}else{
			if(langCur === 'cn'){
				return newDay + '天' + newHour + '小时' + newMin + '分钟'
			}else if(langCur === 'thai'){
				return newDay + 'วัน' + newHour + 'ชั่วโมง' + newMin + 'นาที'
			}else{
				return newDay + 'Days' + newHour + 'Hour' + newMin + 'Minute'
			}
		}
	}
}

export const rmoney = (s,n)=> {
    n = n > 0 && n <= 20 ? n : 2;
    s = parseFloat((s + "").replace(/[^\d\.-]/g, "")).toFixed(n) + "";
    let l = s.split(".")[0].split("").reverse(), r = s.split(".")[1];
    let t = "";
    for (let i = 0; i < l.length; i++) {
        t += l[i] + ((i + 1) % 3 == 0 && (i + 1) != l.length ? "," : "");
    }
    return t.split("").reverse().join("") + "." + r;
}