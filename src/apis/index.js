import axios from 'axios'
var qs = require('qs')
const host = 'http://testapi.thaipaymall.com' //原服务器路径
const newHost = '/api' //本地路径
//const newHost = 'http://localhost:8080' //柜机路径
axios.defaults.timeout = 30000
//统一请求头
const up_data = {
	"sessionContext": {
	    "entityCode": 1,
	    "channel": "1",
	    "serviceCode": "FLEX0001",
	    "postingDateText": "20170215175232",
	    "valueDateText": "20170215175232",
	    "localDateTimeText": "20170215175232",
	    "transactionBranch": "0100001",
	    "userId": "sysadmin",
	    "password": "1",
	    "superUserId": "sysadmin",
	    "superPassword": "1",
	    "authorizationReason": "1",
	    "externalReferenceNo": "C7ABDEA6C33BE75353FA3CE6C88B5C25",
	    "userReferenceNumber": "59CA0BA846317538590CEE59E971CE72",
	    "originalReferenceNo": "C145AE90A39B23A67F7FD333F58958ED",
	    "marketCode": "1",
	    "stepCode": "1",
	    "accessSource": "1",
	    "accessSourceType": "1"
	 }
}

// 根据ID获取帮助详细信息
export function getHelp (supportId) {
  const url = `${host}/support/findById`
  return axios.get(url, { params: { supportId } }).then((res) => {
    return Promise.resolve(res.data)
  })
}

// 根据搜索帮助列表或所有帮助列表
export function searchHelp (data) {
  const url = `${host}/support/find`
  return axios.get(url, { params: data }).then((res) => {
    return Promise.resolve(res.data)
  })
}

// 获取快递柜箱格详细信息
export function getLockerDetail (lockerId) {
  const url = `${host}/lockers/lockerinfo`
  return axios.get(url, { params: {lockerId} }).then((res) => {
    return Promise.resolve(res.data)
  })
}

// 验证取件码取件
export function getGood (code, lockerId) {
  const url = `${host}/delivery/end/${code}`
  return axios.post(url, qs.stringify({ code, lockerId })).then((res) => {
    return Promise.resolve(res.data)
  })
}

// 本地接口----start
// 初始化获取信息
export function getLockerInfo () {
  const url = 'http://localhost:8082/localInfo/lockersIdentity'
  return axios.get(url).then((res) => {
    return Promise.resolve(res.data)
  })
}

// 获取是否关门状态
export function isClose (lockerUnitId) {
  const url = 'http://localhost:8082/lockerUnit/status'
  return axios.get(url, { params: { lockerUnitId } }).then((res) => {
    return Promise.resolve(res.data)
  })
}

// 初始化注册
export function register (serialNumber) {
  const url = 'http://localhost:8082/localInfo/register'
  const config = {
    headers: {'Content-Type': 'application/json'}
  }
  return axios.post(url, {serialNumber}, config).then((res) => {
    return Promise.resolve(res.data)
  })
}

// 离线开柜
export function outLineOpenDoor (data) {
  const url = 'http://localhost:8082/lockerUnit/checkTotpAndOpen'
  const config = {
    headers: {'Content-Type': 'application/json'}
  }
  return axios.post(url, data, config).then((res) => {
    return Promise.resolve(res.data)
  })
}

// 拍照
export function takePhoto (data) {
  const url = 'http://localhost:8082/lockers/webcam/captureAndUpload'
  const config = {
    headers: {'Content-Type': 'application/json'}
  }
  return axios.post(url, data, config).then((res) => {
    return Promise.resolve(res.data)
  })
}
// 本地接口----end

// 验证派件码派件
export function sendGood (lockerUnitId) {
  const url = `${host}/delivery/start/${lockerUnitId}`
  return axios.post(url, qs.stringify({lockerUnitId})).then((res) => {
    return Promise.resolve(res.data)
  })
}

// 发送短信验证码
// mobile userType
export function getOTP (data) {
  const url = `${host}/user/login/sendLoginSms`
  const config = {
    headers: {'Content-Type': 'application/json'}
  }
  return axios.post(url, data, config).then((res) => {
    return Promise.resolve(res.data)
  })
}

// 快递员忘记密码登录
export function codeLogin (data) {
  const url = `${host}/user/login/loginBySms`
  const config = {
    headers: {'Content-Type': 'application/json'}
  }
  return axios.post(url, data, config).then((res) => {
    return Promise.resolve(res.data)
  })
}

// 用户忘记密码登录
export function userCodeLogin (data) {
  const url = `${host}/user/login/loginRegister`
  const config = {
    headers: { 'Content-Type': 'application/json' }
  }
  return axios.post(url, data, config).then((res) => {
    return Promise.resolve(res.data)
  })
}

// ---------------------列表start-------------------
// 查看客户包裹列表
export function checkGetterList (receiverPhoneNO, lockerId) {
  const url = `${host}/delivery/end/forget`
  return axios.get(url, { params: { receiverPhoneNO, lockerId } }).then((res) => {
    return Promise.resolve(res.data)
  })
}

// 查看快递员待派件列表
export function checkSenderList (courierId, lockerId) {
  const url = `${host}/delivery/start/forget`
  return axios.get(url, { params: { courierId, lockerId } }).then((res) => {
    return Promise.resolve(res.data)
  })
}

// 查看快递员待回收包裹列表
export function checkRecycleList (courierId, lockerId) {
  const url = `${host}/delivery/retrieve/parcels`
  return axios.get(url, { params: { courierId, lockerId } }).then((res) => {
    return Promise.resolve(res.data)
  })
}
// ---------------------列表end-----------------

// 获取广告
export function getAd () {
  const url = `${host}/advertise/getadv`
  return axios.get(url).then((res) => {
    return Promise.resolve(res.data)
  })
}

// 标题，广告商，广告载体ID,广告类型Id, 开始时间，结束时间，广告状态
export function searchAd (data) {
  const url = `${host}/advertise/adv`
  return axios.get(url, { params: data }).then((res) => {
    return Promise.resolve(res.data)
  })
}

// 新增报障
export function createFault (data) {
  const url = `${host}/fault/create`
  return axios.post(url, qs.stringify(data)).then((res) => {
    return Promise.resolve(res.data)
  })
}

// 格子报障
export function boxFault (lockersUnitId) {
  const url = `${host}/lockers/lockerunitfault/${lockersUnitId}`
  const config = {
    headers: {'Content-Type': 'application/json'}
  }
  return axios.post(url, { lockersUnitId }, config).then((res) => {
    return Promise.resolve(res.data)
  })
}

// 获取快递员信息
export function getEmployeeInfo (Uid) {
  const url = `${host}/Employee/findEmployeeByUid`
  return axios.get(url, { params: { Uid } }).then((res) => {
    return Promise.resolve(res.data)
  })
}
// ----------------------忘记密码开门start-------------------

// 客户忘记取件码--取件
export function userForgetToGetGood (data) {
  const url = `${host}/delivery/end/forget/open`
  const config = {
    headers: {'Content-Type': 'application/json'}
  }
  return axios.post(url, data, config).then((res) => {
    return Promise.resolve(res.data)
  })
}

// 快递员忘记派件码--派件
export function employeeForgetToSendGood (data) {
  const url = `${host}/delivery/start/forget/open`
  const config = {
    headers: {'Content-Type': 'application/json'}
  }
  return axios.post(url, data, config).then((res) => {
    return Promise.resolve(res.data)
  })
}

// 快递员忘记派件码--回收包裹
export function employeeForgetToRecycleGood (data) {
  const url = `${host}/delivery/retrieve/open`
  const config = {
    headers: { 'Content-Type': 'application/json' }
  }
  return axios.post(url, data, config).then((res) => {
    return Promise.resolve(res.data)
  })
}
// ----------------------忘记密码开门end-------------------

// 选择格子
export function chooseBox (data) {
  const url = `${host}/delivery/start/lockers/choose`
  const config = {
    headers: {'Content-Type': 'application/json'}
  }
  return axios.post(url, data, config).then((res) => {
    return Promise.resolve(res.data)
  })
}

// 取件失败重开柜门
export function reOpenDoor (data) {
  const url = `${host}/delivery/end/reopen`
  const config = {
    headers: {'Content-Type': 'application/json'}
  }
  return axios.post(url, data, config).then((res) => {
    return Promise.resolve(res.data)
  })
}

// 派件后取消订单
export function cancelOrder (data) {
  const url = `${host}/delivery/start/cancel`
  const config = {
    headers: {'Content-Type': 'application/json'}
  }
  return axios.post(url, data, config).then((res) => {
    return Promise.resolve(res.data)
  })
}

// 派件完成后确认
export function sendingSure (data) {
  const url = `${host}/delivery/start/consummation`
  const config = {
    headers: {'Content-Type': 'application/json'}
  }
  return axios.post(url, data, config).then((res) => {
    return Promise.resolve(res.data)
  })
}

// 获取柜机平台帮助类别
export function getHelpType (sourceId) {
  const url = `${host}/supportType/findUsableBySource`
  return axios.get(url, { params: { sourceId } }).then((res) => {
    return Promise.resolve(res.data)
  })
}

// 获取box价格
export function getBoxPrice (companyId) {
  const url = `${host}/commodity/${companyId}/findAllDelivery`
  return axios.post(url).then((res) => {
    return Promise.resolve(res.data)
  })
}

// 关闭柜门生成订单运单
export function createOrder (data) {
  const url = `${host}/delivery/start/closeDoor`
  const config = {
    headers: {'Content-Type': 'application/json'}
  }
  return axios.post(url, data, config).then((res) => {
    return Promise.resolve(res.data)
  })
}

// 获取公司钱包金额
export function getMoney (companyId, token) {
  const url = `${host}/wallet/company/${companyId}/find`
  const config = {
    headers: {'token': token}
  }
  return axios.post(url, {}, config).then((res) => {
    return Promise.resolve(res.data)
  })
}


/*
 * 新接口
 */

//获取储物柜信息
export function getBoxInfo (param) {
  const url = `${newHost}/bes/box/detail/inquiry`
  let up_dataItem = Object.assign({},up_data)
  let newUp = Object.assign(up_dataItem,param)
  return axios.post(url,newUp).then((res) => {
    return Promise.resolve(res.data)
  })
}

//格子信息查询
export function boxCheck (param) {
  const url = `${newHost}/bes/lattice/list/inquiry`
  let up_dataItem = Object.assign({},up_data)
  let newUp = Object.assign(up_dataItem,param)
  return axios.post(url,newUp).then((res) => {
    return Promise.resolve(res.data)
  })
}

//获取广告
export function getNewAd (param) {
  const url = `${newHost}/bes/boxAdv/list/inquery`
  let up_dataItem = Object.assign({},up_data)
  let newUp = Object.assign(up_dataItem,param)
  return axios.post(url,newUp).then((res) => {
    return Promise.resolve(res.data)
  })
}

//添加故障信息
export function addBoxError (param) {
  const url = `${newHost}/bes/boxError/add`
  let up_dataItem = Object.assign({},up_data)
  let newUp = Object.assign(up_dataItem,param)
  return axios.post(url,newUp).then((res) => {
    return Promise.resolve(res.data)
  })
}

//开柜码校验
export function openDoorCodeCheck (param) {
  const url = `${newHost}/cab/pin/code/check`
  let up_dataItem = Object.assign({},up_data)
  let newUp = Object.assign(up_dataItem,param)
  return axios.post(url,newUp).then((res) => {
    return Promise.resolve(res.data)
  })
}

//新创建订单
export function newCreateOrder (param) {
  const url = `${newHost}/bes/order/create/maintenance`
  let up_dataItem = Object.assign({},up_data)
  let newUp = Object.assign(up_dataItem,param)
  return axios.post(url,newUp).then((res) => {
    return Promise.resolve(res.data)
  })
}

//打开柜门
export function openDoor (param) {
  const url = `${newHost}/cab/pin/code/check`
  let up_dataItem = Object.assign({},up_data)
  let newUp = Object.assign(up_dataItem,param)
  return axios.post(url,newUp).then((res) => {
    return Promise.resolve(res.data)
  })
}

//订单状态维护
export function orderStatus (param) {
  const url = `${newHost}/bes/order/status/maintenance`
  let up_dataItem = Object.assign({},up_data)
  let newUp = Object.assign(up_dataItem,param)
  return axios.post(url,newUp).then((res) => {
    return Promise.resolve(res.data)
  })
}

//订单详情查询
export function orderInquiry (param) {
  const url = `${newHost}/bes/order/detail/inquiry`
  let up_dataItem = Object.assign({},up_data)
  let newUp = Object.assign(up_dataItem,param)
  return axios.post(url,newUp).then((res) => {
    return Promise.resolve(res.data)
  })
}

//订单列表查询
export function orderListInquiry (param) {
  const url = `${newHost}/des/order/list/inquiry`
  let up_dataItem = Object.assign({},up_data)
  let newUp = Object.assign(up_dataItem,param)
  return axios.post(url,newUp).then((res) => {
    return Promise.resolve(res.data)
  })
}

//取货码状态维护
export function codeStatus (param) {
  const url = `${newHost}/bes/code/status/maintenance`
  let up_dataItem = Object.assign({},up_data)
  let newUp = Object.assign(up_dataItem,param)
  return axios.post(url,newUp).then((res) => {
    return Promise.resolve(res.data)
  })
}

//测试
export function testCode (param) {
  const url = `${newHost}/cos/send/code/notice`
  let up_dataItem = Object.assign({},up_data)
  let newUp = Object.assign(up_dataItem,param)
  return axios.post(url,newUp).then((res) => {
    return Promise.resolve(res.data)
  })
}