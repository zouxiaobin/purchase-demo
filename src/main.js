// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import VueI18n from 'vue-i18n'
import messages from './common/language/index'
import * as funs from './common/js'
import store from './store/index'
import * as apis from '@/apis/index'
import * as newApis from '@/apis/api'
import filters from './filters'
import md5 from 'js-md5';
import VueLazyLoad from 'vue-lazyload'
import Element from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import vuePicturePreview from 'vue-picture-preview'

Vue.prototype.$ajax = apis
Vue.prototype.$post = newApis
Vue.prototype.$md5 = md5;
Vue.config.productionTip = false
Vue.prototype.productionEnv = false  //部署环境判断，true为生产环境，false为测试环境
Vue.use(VueI18n)
Vue.use(filters)
Vue.use(Element)
Vue.use(vuePicturePreview)

const lang = funs.lang()
window.lang = funs.lang()
const i18n = new VueI18n({
  locale: lang,
  messages
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  i18n,
  store,
  components: { App },
  template: '<App/>'
})
