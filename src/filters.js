var filterPlugin = {}

function formatDate (date, fmt) {
  var o = {
    'M+': date.getMonth() + 1, // 月份
    'd+': date.getDate(), // 日
    'h+': date.getHours(), // 小时
    'm+': date.getMinutes(), // 分
    's+': date.getSeconds(), // 秒
    'q+': Math.floor((date.getMonth() + 3) / 3), // 季度
    'S': date.getMilliseconds() // 毫秒
  }
  if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (date.getFullYear() + '').substr(4 - RegExp.$1.length))
  for (var k in o) {
    if (new RegExp('(' + k + ')').test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length === 1) ? (o[k]) : (('00' + o[k]).substr(('' + o[k]).length)))
  }
  return fmt
}

filterPlugin.install = (Vue, options) => {
  Vue.filter('dealStatus', (value, format) => {
    let r
    if (window.lang === 'cn') {
      r = {
        '0': '未处理',
        '1': '处理中',
        '2': '已完成'
      }
    } else if (window.lang === 'en') {
      r = {
        '0': 'No item to manage',
        '1': 'It is now processing',
        '2': 'Completed'
      }
    } else {
      r = {
        '0': 'ไม่มีการจัดการ',
        '1': 'กำลังจัดการ',
        '2': 'สำเร็จ'
      }
    }
    return r[value]
  })
  Vue.filter('faultPlatform', (value, format) => {
    let r
    if (window.lang === 'cn') {
      r = {
        '0': '人工',
        '1': '设备端',
        '2': '安卓客户端',
        '3': 'IOS客户端'
      }
    } else if (window.lang === 'en') {
      r = {
        '0': 'Administrator staff',
        '1': '设备端',
        '2': 'Android Clien',
        '3': 'IOS Clien'
      }
    } else {
      r = {
        '0': 'พนักงาน',
        '1': '设备端',
        '2': 'ลูกค้าแอนดรอย',
        '3': 'ลูกค้าIOS'
      }
    }
    return r[value]
  })
  Vue.filter('canUse', (value, format) => {
    let r
    if (window.lang === 'cn') {
      r = {
        '0': '不可用',
        '1': '可用'
      }
    } else if (window.lang === 'en') {
      r = {
        '0': '不可用',
        '1': '可用'
      }
    } else {
      r = {
        '0': '不可用',
        '1': '可用'
      }
    }
    return r[value]
  })

  Vue.filter('adStatus', (value, format) => {
    let r
    if (window.lang === 'cn') {
      r = {
        '0': '不可用',
        '1': '正在使用'
      }
    } else if (window.lang === 'en') {
      r = {
        '0': 'can not use',
        '1': 'Using'
      }
    } else {
      r = {
        '0': 'ไม่สามารถใช้ได้',
        '1': 'กำลังใช้งาน'
      }
    }
    return r[value]
  })

  Vue.filter('superAdminName', (value, format) => {
    let r
    if (window.lang === 'cn') {
      r = {
        '0': '普通管理员',
        '1': '超级管理员',
        '2': '运营',
        '3': '发货员',
        '4': '客服'
      }
    } else if (window.lang === 'en') {
      r = {
        '0': 'Admin',
        '1': 'Senior Admin',
        '2': 'Operate',
        '3': 'Delivery staff',
        '4': 'Customer service'
      }
    } else {
      r = {
        '0': 'ผู้ดูแลทั่วไป',
        '1': 'ผู้ดูแลระดับสูง',
        '2': 'การดำเนินงาน',
        '3': 'พนักงานจัดส่งพัสดุ',
        '4': 'บริการลูกค้า'
      }
    }
    return r[value]
  })

  Vue.filter('superAdminRight', (value, format) => {
    let r
    if (window.lang === 'cn') {
      r = {
        '0': '除了版本管理和权限管理的其他模块',
        '1': '全部',
        '2': '广告详情',
        '3': '快递发货',
        '4': '重发派件码，重发取件码'
      }
    } else if (window.lang === 'en') {
      r = {
        '0': 'Other modules which except version management and rights management',
        '1': 'All',
        '2': 'More Advertisement detail',
        '3': 'Sending Packages',
        '4': 'Send your delivery code again，Send  your pick-up code again'
      }
    } else {
      r = {
        '0': 'นอกจากดูแลเวอร์ชั่นและระบบการดูแลของโมดูลอ',
        '1': 'ทั้งหมด',
        '2': 'รายละเอียดโฆษณา',
        '3': 'จัดส่งสินค้า',
        '4': 'ส่งรหัสส่งพัสดุอีกครั้ง ส่งรหัสรับพัสดุอีกค'
      }
    }
    return r[value]
  })

  Vue.filter('date', (date, format) => {
    if (!date) return ''
    if (!/\.000Z$/.test(date)) {
      date += '.000Z'
    }
    if (date !== 'null.000Z') return formatDate(new Date(date), format || 'yyyy-MM-dd hh:mm:ss')
    else return ''
  })

  Vue.filter('error', (value, format) => {
    let r
    if (window.lang === 'cn') {
      r = {
        '0': '',
        '1': '',
        '2': '*该输入框的值不能为空',
        '3': '*请选择',
        '4': '格式错误'
      }
    } else if (window.lang === 'en') {
      r = {
        '0': '',
        '1': '',
        '2': '*该输入框的值不能为空',
        '3': '*请选择',
        '4': '格式错误'
      }
    } else if (window.lang === 'thai') {
      r = {
        '0': '',
        '1': '',
        '2': '*该输入框的值不能为空',
        '3': '*请选择',
        '4': '格式错误'
      }
    }
    return r[value]
  })
}
export default filterPlugin
